import React from "react";
import {
  StylesProvider,
  ThemeProvider,
  createMuiTheme,
} from "@material-ui/core/styles";

// Configure JSS
// const rtljss = create({ plugins: [...jssPreset().plugins, rtl()] });

const ltrTheme = createMuiTheme({ direction: "ltr" });
const rtlTheme = createMuiTheme({ direction: "rtl" });
type RtlProviderProps = {
  children: React.ReactNode;
};

export default function Direction(props: RtlProviderProps) {
  const [isRtl] = React.useState(false);
  const { children } = props;

  return (
    <ThemeProvider theme={isRtl ? rtlTheme : ltrTheme}>
      <StylesProvider>
        {children}
      </StylesProvider>
    </ThemeProvider>
  );
}
