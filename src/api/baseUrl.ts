import axios from "axios";

let baseURL = process.env.NEXT_PUBLIC_AUTH_BASE_URL;
export let authInstance = axios.create({
  baseURL: process.env.NEXT_PUBLIC_AUTH_BASE_URL,
  headers: {
    Accept: "application/json",
  },
});

console.log("baseURL:", baseURL);
