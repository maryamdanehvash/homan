import React, { useState } from "react";
import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalBody,
  Button,
  useDisclosure,
  Box,
} from "@chakra-ui/react";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import { Form, Formik } from "formik";
import { InputField } from "../components/InputField";
import { Wrapper } from "../components/Wrapper";
import { useStyles } from "../styles/styles";
import { Container, IconButton } from "@material-ui/core";
import { ModalHeader } from "@chakra-ui/modal";
// import { authInstance } from "../api/baseUrl";
import axios from "axios";

interface indexProps {}
const initialValues = {
  // username: "",
  phoneNumber:"",
  password:""
};
const initialValuesOtp = {
  // authentication_type:"",
  // username: "",
  value: "",
};

const Index: React.FC<indexProps> = ({}) => {
  const [errors, setErrors] = useState();

  const {
    isOpen: isOpenLogin,
    onOpen: onOpenLogin,
    onClose: onCloseLogin,
  } = useDisclosure();
  const {
    isOpen: isOpenOtp,
    onOpen: onOpenOtp,
    onClose: onCloseOtp,
  } = useDisclosure();
  // const {
  //   isOpen: isOpenPass,
  //   onOpen: onOpenPass,
  //   onClose: onClosePass,
  // } = useDisclosure();

  const classes = useStyles();
  let body: any = null;
  let bodyPass: any = null;
  const handelOnSubmit = async (values: any) => {
    await axios({
      method: "POST",
      url: "https://testcustomer.linkeee.ir/v1/Account/SignIn",
      data: values,
    })
      .then((response) => {
        console.log(response.data);
      })
      .catch((err) => {
        const error = err.response.data;
        console.log(error);
        // setErrors(error);
      });
  };

  return (
    <Container>
      <IconButton
        className={classes.icon}
        onClick={onOpenLogin}
        // component={onOpenLogin}
        // to={`/url`}
      >
        <AccountCircleIcon className={classes.loginIcon} />
      </IconButton>

      <Modal isOpen={isOpenLogin} onClose={onCloseLogin} size="xl">
        <ModalOverlay />

        <ModalContent textAlign="center" fontSize="md" color="orchid">
          <ModalHeader>شماره موبایل خود را وارد نمایید</ModalHeader>
          <ModalBody pb={1}>
            <Formik initialValues={initialValues} onSubmit={handelOnSubmit}>
              {({ isSubmitting }) => (
                <Wrapper>
                  <Form>
                    <InputField
                      label="موبایل"
                      name="phoneNumber"
                      error={errors ? true : false}
                      helperText={errors}
                    />
                    <InputField
                      label="رمز عبور"
                      name="password"
                      error={errors ? true : false}
                      helperText={errors}
                    />
                    <Box mt={6}>
                      <Button
                        isFullWidth={true}
                        isLoading={isSubmitting}
                        type={"submit"}
                        colorScheme="purple"
                        isOpenOtp={isOpenOtp}
                        // error={errors ? true : false}
                        onClick={onOpenOtp}
                      >
                        ارسال
                      </Button>
                    </Box>
                    {body}
                  </Form>
                </Wrapper>
              )}
            </Formik>
          </ModalBody>
        </ModalContent>
      </Modal>
    </Container>
  );
};

export default Index;
