import { ChakraProvider } from "@chakra-ui/react";
import theme from "../theme";
import { AppProps } from "next/app";
import React from "react";
import { create } from "jss";
import rtl from "jss-rtl";
import {
  StylesProvider,
  jssPreset,
  ThemeProvider,
} from "@material-ui/core/styles";
import {rtlTheme, ltrTheme} from '../styles/MuiTheme';

const jss = create({ plugins: [...jssPreset().plugins, rtl()] });

// type RtlProviderProps = {
//   children: React.ReactNode;
// };

function MyApp({ Component, pageProps }: AppProps) {
  const [isRtl] = React.useState(true);
  // React.useLayoutEffect(() => {
  //   document.body.setAttribute("dir", isRtl ? "rtl" : "ltr");
  // }, [isRtl]);
  // const { children } = props;
  return (
    <ChakraProvider resetCSS theme={theme}>
      <ThemeProvider theme={isRtl ? rtlTheme : ltrTheme}>
        <StylesProvider jss={jss}>
          <Component {...pageProps} />
        </StylesProvider>
      </ThemeProvider>
      {/* <Button onClick={() => setIsRtl(!isRtl)}>Toggle direction</Button> */}
    </ChakraProvider>
  );
}

export default MyApp;
